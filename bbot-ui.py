#!/usr/bin/env python3

import sys
import string
import socket
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtNetwork import QTcpSocket
from ui import mainwindow

# HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 23

# with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#     s.connect((HOST, PORT))
#     s.sendall(b'Hello, world')
#     data = s.recv(1024)

# print('Received', repr(data))

class ExampleApp(QtWidgets.QMainWindow, mainwindow.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.bbotAddress = "192.168.3.47"
        self.setupUi(self)
        self.socket = QTcpSocket(self)
        self.socket.readyRead.connect(self.onDataReceived)
        self.actionConnect.triggered.connect(self.bbotConnect)
        self.actionDisconnect.triggered.connect(self.bbotDisconnect)
        self.actionSet_address.triggered.connect(self.bbotSetAddress)
        self.commandLine.returnPressed.connect(self.onCommandEntered)
        self.statusbar.showMessage(self.bbotAddress)
    
    def bbotConnect(self):
        self.statusbar.showMessage("Connecting to "+self.bbotAddress)
        if self.socket.state() != QTcpSocket.UnconnectedState:
            return
        self.socket.connectToHost(self.bbotAddress, PORT)
        if not self.socket.waitForConnected(1000):
            self.statusbar.showMessage("Failed to connect to "+self.bbotAddress)
            self.socket.disconnectFromHost()
            return
        self.actionConnect.setEnabled(False)
        self.actionDisconnect.setEnabled(True)
        self.actionSet_address.setEnabled(False)
        self.statusbar.showMessage("Connected to "+self.bbotAddress)

    def bbotDisconnect(self):
        if self.socket.state() != QTcpSocket.UnconnectedState:
            self.socket.disconnectFromHost()
        self.actionConnect.setEnabled(True)
        self.actionDisconnect.setEnabled(False)
        self.actionSet_address.setEnabled(True)
        self.statusbar.showMessage("Disconnected")

    def bbotSetAddress(self):
        text, ok = QtWidgets.QInputDialog.getText(self, 'Set connection address', 'BBot address', text=self.bbotAddress)
        if ok:
            text = str(text)
            text.translate({ord(c): None for c in string.whitespace})
            self.bbotAddress = text
            self.statusbar.showMessage("New address "+self.bbotAddress)

    def onCommandEntered(self):
        command = str(self.commandLine.text()).strip()
        commandSend = command + "\r\n"
        if self.socket.state() == QTcpSocket.ConnectedState:
            self.socket.write(commandSend.encode())
            self.statusbar.showMessage(command)
        self.commandLine.setText("")

    @pyqtSlot()
    def onDataReceived(self):
        while self.socket.canReadLine():
            outLine = str(self.socket.readLine().data().decode().strip())
            if len(outLine) == 0:
                continue
            if outLine[0] == '$':
                outLine = outLine[1:].strip()
                self.traceOutput.appendPlainText(outLine)
            else:
                self.commandOutput.appendPlainText(outLine)

def main():
    app = QtWidgets.QApplication(sys.argv)
    window = ExampleApp()
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()